require('packer_plugins_to_install')
require('vim_plug_plugins_to_install')
require('appearance.appearance')
require('miscellaneous.misc')
require('miscellaneous.markdown')
require('navigation.telescope')
require('lsp.coc')
require('dap.dap')
-- Needed dependency for "dap-python": debugpy
-- Note:
-- When running DapContinue for python the debugger starts
-- by default at the current directory of the current python script.
require("dap-python").setup("python")
-- Needed dependency for lazygit.nvim: lazygit
require("lazygit.lazygit")
-- This will load the launch.json file from your project's .vscode directory
require('dap.ext.vscode').load_launchjs(nil, {python = { 'python' } })
require('nvim-tree').setup()
require('nvim-tree.config')
