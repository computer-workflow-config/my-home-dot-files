-- Add numbers to each line on the left-hand side.
vim.o.number = true

-- Vundle setup
vim.cmd('set nocompatible')              -- be iMproved, required
vim.cmd('filetype off')                  -- required

-- Highlight cursor line underneath the cursor horizontally.
-- vim.cmd('set cursorline')

-- Highlight cursor line underneath the cursor vertically.
-- vim.cmd('set cursorcolumn')

-- Link these styles so they look the same
-- vim.cmd('hi clear CursorLine')
-- vim.cmd('hi link CursorLine CursorColumn')

-- Link line numbers rows style so they look the same as CursorColumn
-- vim.cmd('hi clear CursorLineNr')
-- vim.cmd('hi link CursorLineNr CursorColumn')

-- show current file name in status bar
vim.cmd('set laststatus=2')
vim.cmd('set statusline=%f "tail of the filename')

-- Turn syntax highlighting on.
vim.cmd('syntax enable')

-- Set relative number.
vim.cmd('set relativenumber')
