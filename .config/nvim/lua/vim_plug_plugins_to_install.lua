-- Download vim-plug if not already installed                   
local fn = vim.fn                                               
local install_path = fn.stdpath('data')..'/site/autoload/plug.vim'                                             
if fn.empty(fn.glob(install_path)) > 0 then
   fn.system({'curl', '-fLo', install_path, '--create-dirs','https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'})
end                                                             

vim.cmd [[call plug#begin()]]
vim.cmd [[Plug 'junegunn/vim-plug']] -- use: CocInstall coc-pyright to use coc for python
vim.cmd [[Plug 'neoclide/coc.nvim', {'branch': 'release'}]]
vim.cmd [[Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }]] -- To have fzf in nvim (ref: https://github.com/junegunn/fzf.vim) example of a command :FZF
vim.cmd [[Plug 'junegunn/fzf.vim']]
vim.cmd [[call plug#end()]]

-- Do not forget to use PlugInstall each time you add a new plugin
