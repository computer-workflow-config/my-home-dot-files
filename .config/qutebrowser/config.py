# Load autoconfig (optional, useful if you use `:set` in qutebrowser to adjust settings)
config.load_autoconfig(False)

# Appearance
c.zoom.default = 200  # Default zoom level, e.g., 1.0 for 100%
c.fonts.default_size = "17pt"  # Default font size
#c.tabs.show = "multiple"  # Show tabs only if multiple are open
#c.statusbar.show = "always"  # Show status bar at the bottom

# Privacy and Security
#c.content.cookies.accept = "no-unknown-3rdparty"  # Block 3rd-party cookies
#c.content.headers.do_not_track = True  # Enable Do Not Track
#c.content.blocking.enabled = True  # Enable ad-blocking
#c.content.geolocation = False  # Disable location requests

# Start Page
#c.url.start_pages = "https://example.com"  # Set start page
#c.url.default_page = "https://example.com"  # Set default page for new tabs

# Search Engines
#c.url.searchengines = {
#    "DEFAULT": "https://duckduckgo.com/?q={}",  # Default search engine
#    "g": "https://www.google.com/search?q={}",  # Shortcut for Google
#    "w": "https://en.wikipedia.org/wiki/{}",  # Shortcut for Wikipedia
#}

# Key Bindings (examples)
#config.bind("J", "tab-prev")  # Bind 'J' to switch to the previous tab
#config.bind("K", "tab-next")  # Bind 'K' to switch to the next tab
#config.bind("d", "tab-close")  # Bind 'd' to close the current tab

# Downloads
#c.downloads.location.directory = "~/Downloads"  # Set download directory

# Auto save session
#c.auto_save.session = True  # Auto-save open tabs on close
