import XMonad

import XMonad.Util.EZConfig
import XMonad.Layout.ThreeColumns
import XMonad.Layout.Magnifier
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.DynamicLog
import XMonad.Util.Run
import XMonad.Layout.Spacing
import XMonad.Layout.NoBorders

import XMonad.Hooks.ManageDocks

import XMonad.Config.Azerty
import XMonad.Util.SpawnOnce


import XMonad.Util.Cursor
-- Below is a variation of the above except no borders are applied
-- if fewer than two windows. So a single window has no gaps.
mySpacing i = spacingRaw True (Border i i i i) True (Border i i i i) True


myLayout =mySpacing 7 . smartBorders $ tiled ||| Mirror tiled ||| Full ||| threeCol
  where
    threeCol = magnifiercz' 1.3 $ ThreeColMid nmaster delta ratio
    tiled    = Tall nmaster delta ratio
    nmaster  = 1      -- Default number of windows in the master pane
    ratio    = 1/2    -- Default proportion of screen occupied by master pane
    delta    = 3/100  -- Percent of screen to increment by when resizing panes

toggleTransparency i=spawn("compton --inactive-opacity "++i++" || pkill compton")


myKeys :: [(String, X ())]
myKeys =
        [ ("C-M1-l", spawn "slock"             ) --M means mode S means shift M1 means left alt C means control
        , ("C-M1-f"  , spawn "firefox"         )
	, ("C-M1-e"  , spawn "emacs"         )
        , ("C-M1-g"  , spawn "gnome-control-center")
        , ("C-M1-n"  , spawn "nautilus")
        , ("C-M1-<Space>", toggleWindowSpacingEnabled)
        , ("C-S-t", toggleTransparency "0.75")
        ]
--myWorkspaces    = map show [1..9]

myStartupHook :: X ()
myStartupHook = do
    setDefaultCursor  xC_left_ptr
    --spawn ("sleep 2 && conky -c $HOME/.config/conky/xmonad/" ++ colorScheme ++ "-01.conkyrc")
    spawnOnce "feh --bg-scale ~/Wallpaper_directory/3471965.jpg &"
    --spawnOnce "/usr/bin/emacs --daemon" -- emacs daemon for the emacsclient
    --spawnOnce "compton --active-opacity 1 &" --0.75
    
main :: IO ()
main = do
  xmproc0 <- spawnPipe "xmobar -x 0 $HOME/.config/xmobar/xmobarrc"
  xmproc1 <- spawnPipe "xmobar -x 1 $HOME/.config/xmobar/xmobarrc"
  xmproc2 <- spawnPipe "xmobar -x 2 $HOME/.config/xmobar/xmobarrc"
  xmonad . ewmh $ def
            { modMask    = mod4Mask  -- Rebind Mod to the Super key
            , focusedBorderColor = "#ff7f00"
            , manageHook         = manageDocks <+> manageHook def --(https://unix.stackexchange.com/questions/288037/xmobar-does-not-appear-on-top-of-window-stack-when-xmonad-starts)
            , layoutHook = avoidStruts $ myLayout -- Use custom layouts
	    , handleEventHook    = handleEventHook def <+> docksEventHook -- note both avoid struts and docs eventhooks are needed (https://unix.stackexchange.com/questions/288037/xmobar-does-not-appear-on-top-of-window-stack-when-xmonad-starts)
            --, workspaces = myWorkspaces
            , logHook =dynamicLogWithPP $  xmobarPP {ppOutput = \x -> hPutStrLn xmproc0 x  >> hPutStrLn xmproc1 x  >> hPutStrLn xmproc2 x
	    }-- def or dzenPP or xmobarPP or sjanssenPP or byorgeyPP
	    , keys = \c -> azertyKeys c <+> keys def c
            ,startupHook        = myStartupHook
            }`additionalKeysP` myKeys

