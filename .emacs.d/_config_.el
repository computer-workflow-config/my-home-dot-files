;;Copy this file: cp _config_.el config.el
;;Then modify the content of config.el if needed
;;Do not put sensitive data in _config_.el but rather in config.el if needed (because config.el is not versioned)

;;The following code is needed for auctex (reference: "https://www.emacswiki.org/emacs/ExecPath")
;;Preliminary requirement: download auctex
(setq auctex_1_string "path_to_texlive_with_:at_start")
(setq auctex_2_string "path_to_texlive")

(setq which_python "python")

(setq org-ai-openai-api-token "");;token:...
