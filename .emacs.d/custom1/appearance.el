;;Disable splash screen and startup message
(setq inhibit-startup-message t)

;;Get a totally empty scratch on startup
(setq initial-scratch-message "")

;;To disable the menu bar
(menu-bar-mode -1) 

;;To disable the toolbar
(tool-bar-mode -1) 

;;Add line numbers
(setq display-line-numbers-type 'relative)
(global-display-line-numbers-mode 1)

;;Set font size
(set-face-attribute 'default nil :height 120)

(defun my-inhibit-global-linum-mode ()
  "Counter-act `global-linum-mode'."
  (add-hook 'after-change-major-mode-hook
            (lambda () (linum-mode 0))
            :append :local))

;;Inhibit global linum mode in doc-view-mode
(add-hook 'doc-view-mode-hook 'my-inhibit-global-linum-mode)

;;Inhibit global linum mode in vterm-mode
(add-hook 'vterm-mode-hook 'my-inhibit-global-linum-mode)


;;Load theme
(use-package doom-themes
  :ensure t ;:ensure keyword makes use-package ask the Emacs package manager to install a package if it is not already present on your system. 
  :config ;:config can be used to execute code after a package is loaded
  ;; Global settings (defaults)
  (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
        doom-themes-enable-italic t) ; if nil, italics is universally disabled
  (load-theme 'doom-solarized-dark t) ;load theme

  ;; Enable flashing mode-line on errors
  (doom-themes-visual-bell-config)
  ;; Enable custom neotree theme (all-the-icons must be installed!)
  (doom-themes-neotree-config)
  ;; or for treemacs users
  (setq doom-themes-treemacs-theme "doom-colors") ; use "doom-atom" for more minimal icon theme
  (doom-themes-treemacs-config)
  ;; Corrects (and improves) org-mode's native fontification.
  (doom-themes-org-config))
;;Reference for doom-themes: https://github.com/doomemacs/themes

;;doom-themes treemacs integration requires all-the-icons and to install fonts
(use-package all-the-icons
  :ensure t
  :config
  (unless (member "all-the-icons" (font-family-list))
    (all-the-icons-install-fonts t))
  )

;;Disabling scrollbars on newly created frames, immediately after they're created
(add-hook 'after-make-frame-functions
          '(lambda (frame)
             (modify-frame-parameters frame
                                      '((vertical-scroll-bars . nil)
                                        (horizontal-scroll-bars . nil)))))

;;To disable the scrollbar
(toggle-scroll-bar -1)

;;dired-hide-dotfiles-mode is package which hides dot files in dired
(use-package dired-hide-dotfiles
  :ensure t
  :config
  (dired-hide-dotfiles-mode t)
  )

;;Mouse click open folder in the same window in Dired mode. Reference:"https://emacs.stackexchange.com/questions/35536/dired-mouse-click-open-folder-in-the-same-window"
;;Additional reference: "https://stackoverflow.com/questions/30989838/symbols-value-as-variable-is-void-dired-mode-map"
;;Add dired-hide-dotfiles-mode keybinding also
(eval-after-load "dired" '(progn
			    (define-key dired-mode-map [mouse-2] 'dired-mouse-find-file)
			    (define-key dired-mode-map [f7] 'dired-hide-dotfiles-mode)))



 
;;transparency emacs (Reference: "https://www.emacswiki.org/emacs/TransparentEmacs")
;;Syntax: (set-frame-parameter (selected-frame) 'alpha '(<active> . <inactive>))
;;Syntax: (set-frame-parameter (selected-frame) 'alpha <both>)
;;(set-frame-parameter (selected-frame) 'alpha '(100 . 100))
;;(add-to-list 'default-frame-alist '(alpha . (100 . 100)))


;;Add icon to dired
(use-package all-the-icons-dired
  :ensure t
  :config
  (add-hook 'dired-mode-hook 'all-the-icons-dired-mode)
  )




;;Enable vertico
;;Preliminary requirement: download vertico from elpa
(use-package vertico
  :ensure t
  :init
  (vertico-mouse-mode))

