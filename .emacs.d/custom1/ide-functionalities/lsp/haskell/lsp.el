(use-package lsp-haskell
   :ensure t
   :init
    (use-package haskell-mode
    :ensure t
    :config
    (add-hook 'haskell-mode-hook #'lsp)
    (add-hook 'haskell-literate-mode-hook #'lsp)
    )
    )

