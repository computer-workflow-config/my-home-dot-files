;;pylsp language server
;;ref: https://emacs-lsp.github.io/lsp-mode/page/lsp-pylsp/
;;ref: https://github.com/python-lsp/python-lsp-server
;;you can install it directly using os distribution package manager
;;or conda install
;; then run "pylsp &" in the terminal and now pylsp is up and running

;;pyright language server
(use-package lsp-pyright
  :ensure t
  :hook (python-mode . (lambda ()
                          (require 'lsp-pyright)
                          (lsp))))  ; or lsp-deferred
