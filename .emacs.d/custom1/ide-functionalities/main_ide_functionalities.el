;;Use Flycheck which is a modern on-the-fly syntax checking extension for GNU Emacs
(use-package flycheck
   :ensure t
   :config
   )

;;Download treemacs package
(use-package treemacs
  :ensure t
  :config
  (global-set-key [f8] 'treemacs);;show/hide treemacs
  )

;;Add evil mode
;;(require 'evil)
;;(evil-mode 1)
;;(add-to-list 'evil-emacs-state-modes 'vterm-mode)
