;;To use latex
;;https://github.com/jwiegley/use-package/issues/379
(use-package tex
  :defer t
  :ensure auctex
  :config
  (setq TeX-auto-save t))

;; ;;Download auctex package
;; (use-package auctex
;;   :ensure t
;;   :config
;;   ;;The following code is needed for auctex (reference: "https://www.emacswiki.org/emacs/ExecPath")
;;   (setenv "PATH" (concat (getenv "PATH") auctex_1_string));;(setq auctex_1_string "path_to_texlive_with_:at_start") in config.el 
;;   (setq exec-path (append exec-path '(auctex_2_string)));;(setq auctex_2_string "path_to_texlive") in config.el 
;;   )

