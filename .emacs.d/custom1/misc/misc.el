;;Change window by hovering mouse
(setq mouse-autoselect-window t)

;;change font_size
(set-face-attribute 'default nil :height 200)

;;Open pdf using evince
(use-package openwith
  :ensure t
  :config
  (setq openwith-associations '(("\\.pdf\\'" "evince" (file))))
  (openwith-mode t)
  )

;;emacs magit
(use-package magit
  :ensure t
  )

;;window move (shift + arrow key)
(windmove-default-keybindings)

(defun my/scroll-down-half-page ()
  "scroll down half a page while keeping the cursor centered"
  (interactive)
  (let ((ln (line-number-at-pos (point)))
    (lmax (line-number-at-pos (point-max))))
    (cond ((= ln 1) (move-to-window-line nil))
      ((= ln lmax) (recenter (window-end)))
      (t (progn
           (move-to-window-line -1)
           (recenter))))))



(defun my/scroll-up-half-page ()
  "scroll up half a page while keeping the cursor centered"
  (interactive)
  (let ((ln (line-number-at-pos (point)))
    (lmax (line-number-at-pos (point-max))))
    (cond ((= ln 1) nil)
      ((= ln lmax) (move-to-window-line nil))
      (t (progn
           (move-to-window-line 0)
           (recenter))))))



;;Vim keybindings in emacs
(use-package evil
  :ensure t
  :config
  (evil-mode 1)
  (global-set-key (kbd "C-u") 'my/scroll-up-half-page)
  (global-set-key (kbd "C-d") 'my/scroll-down-half-page)
  )

;;Treat the underscore as a word constituent 
;;reference: https://stackoverflow.com/questions/41681167/modify-syntax-entry-is-not-loaded-in-emacs-init
(defun my-syntax () (modify-syntax-entry ?_ "w" (syntax-table)))
(add-hook 'after-change-major-mode-hook 'my-syntax)

;;Use the commonly bound keys familiar to most people today. Ctrl+C for copy, Ctrl+z for undo, etc
;;(use-package ergoemacs-mode
;;  :ensure t
;;  :config
;;  (ergoemacs-mode 1)
;;  )


;;Add right click context
;;Preliminary requirement: download right-click-context from elpa
;;(right-click-context-mode 1)

;; (use-package right-click-context
;;   :ensure t
;;   :config
;;   (right-click-context-mode 1)
;;   )

;;Add markdown-mode
(use-package markdown-mode
  :ensure t
  )

;;Add markdown-preview-mode
;;Preliminary requirement: download and install markdown-preview-mode


;;show-paren-mode allows one to see matching pairs of parentheses and other characters
;;M-x show-paren-mode RET

;;Add haskell-mode
;;Preliminary requirement: download haskell-mode

;;The ido.el package lets you interactively do things with buffers and files
(use-package ido
  :config
  (ido-mode t)
  )

;;Smex is a M-x enhancement for Emacs
(use-package smex
  :ensure t
  :config
  ;;(global-set-key (bd "M-x") 'smex)
  ;;(global-set-key (kbd "M-X") 'smex-major-mode-commands)
  ;; This is your old M-x.
  ;;(global-set-key (kbd "C-c C-c M-x") 'execute-extended-command)
  )

;;fzf package for emacs

(defun my/fzf-local-dir()
  "dired using fzf"
  (interactive)
  (setq command (concat "cd " (my/pwd) "&& find . -type d"))
  (setq action 'dired)
  (fzf-with-command command action)
  )

(defun my/fzf()
  "fzf starting from the home directory"
  (defun my/find-file-home (file)
    "Open FILE in the home directory."
    (find-file (expand-file-name file "~")))
  (interactive)
  (setq command "cd ~ && find .")
  (setq action 'my/find-file-home)
  (fzf)
  (fzf-with-command command action)
  )

(defun cdf()
  "Alias for my/fzf"
  (interactive)
  (my/fzf)
  )

(defun create-directory-if-not-exists (directory)
  "Create DIRECTORY if it doesn't exist."
  (unless (file-directory-p directory)
    (make-directory directory t)))

(defun my/launch-separate-emacs-in-terminal ()
  "Launch a separate emacs in the terminal knowing that the current emacs is run in the terminal"
  (interactive)
  (suspend-emacs "fg ; emacs -nw"))

(defun my/launch-separate-emacs-under-x ()
  "Launch a separate gui emacs"
  (interactive)
  (call-process "sh" nil nil nil "-c" "emacs &"))

(defun my/restart-emacs ()
  "Restart emacs"
  (interactive)
  ;; We need the new emacs to be spawned after all kill-emacs-hooks
  ;; have been processed and there is nothing interesting left
  (let ((kill-emacs-hook (append kill-emacs-hook (list (if (display-graphic-p)
                                                           #'my/launch-separate-emacs-under-x
                                                         #'my/launch-separate-emacs-in-terminal)))))
    (save-buffers-kill-emacs)))

;;Keybindings below
(evil-leader/set-key
    "<SPC>" 'smex
    "X" 'smex-major-mode-commands
    "b" 'ido-switch-buffer
    "d" 'dired
    "f" 'fzf
)
;; note the F1 button is another alias to the C-h button for help commands

