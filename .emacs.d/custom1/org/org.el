(use-package org
  :ensure t
  :config
  (use-package org-bullets
    :ensure t
    :config
    (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))
    )
  (use-package epresent
    :ensure t
    :config
    )
  (setq org-log-done 'time) 
  ;;(setq org-log-done 'note)
  (create-directory-if-not-exists "~/org")
  (setq org-agenda-files (list "~/org"))
  )

;; Note: org folding and unfolding in evil mode only work in insert mode
