(defun my/insert-shell-command-output ()
  (interactive)
  (insert (shell-command-to-string (read-string "Enter the shell command to insert its output:"))))

;;Reference: https://www.emacswiki.org/emacs/RecreateScratchBuffer
(defun my/recreate-scratch-buffer nil
       "create/recreate a scratch buffer"
       (interactive)
       (when (get-buffer "*scratch*")
	 (kill-buffer "*scratch*"))
       (switch-to-buffer (get-buffer-create "*scratch*"))
       (lisp-interaction-mode))



(defun my/insert-shell-command-output-into-scratch ()
  (interactive)
  (setq input (read-string "Enter the shell command to insert its output into *scratch*:"))
  (my/recreate-scratch-buffer)  
  (insert (shell-command-to-string input))
)

(defun my/get-shell-report()
  (interactive)
  (setq input (read-string "Enter the shell command to get output as read only buffer:"))
  (my/recreate-scratch-buffer)  
  (insert (shell-command-to-string input))
  (read-only-mode)
)


;;The default shell-command gives its output to a new buffer
