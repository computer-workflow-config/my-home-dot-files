(defun my/pwd ()
  "Remove term Directory and /ssh:... from pwd"
  (interactive)
  (replace-regexp-in-string "Directory \\(/ssh:[^:]+:\\)?" "" (pwd)))
