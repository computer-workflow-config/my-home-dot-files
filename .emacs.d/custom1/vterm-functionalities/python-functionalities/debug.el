(defun my/vterm-pdb-from-current ()
  "Debug current python script in vterm and using pdb"
  (interactive)
  (buffer-file-name (other-buffer))
  (my/vterm-execute-command "source .venv/bin/activate")
  (my/vterm-execute-command (concat "python -m pdb " buffer-file-name))  
  )

(advice-add 'my/vterm-pdb-from-current :around #'my/vterm-other-window-decorator)
