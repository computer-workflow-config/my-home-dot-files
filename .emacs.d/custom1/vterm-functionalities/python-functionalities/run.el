(defun my/vterm-python-from-current ()
  "Python current python script in vterm"
  (interactive)
  (buffer-file-name (other-buffer))
  (my/vterm-execute-command "source .venv/bin/activate")
  (my/vterm-execute-command (concat "python " buffer-file-name))  
  )

(advice-add 'my/vterm-python-from-current :around #'my/vterm-other-window-decorator)
