(defun my/venv-create ()
  "Create .venv environement in the current directory using vterm"
  (interactive)
  (my/vterm-execute-command (concat which_python " -m venv .venv"));;(setq which_python "python_path") in config.el
)

(advice-add 'my/venv-create :around #'my/vterm-other-window-decorator)

(defun my/venv-activate ()
  "Create .venv environement in the current directory using vterm"
  (interactive)
  (my/vterm-execute-command "source .venv/bin/activate")
)

(advice-add 'my/venv-activate :around #'my/vterm-other-window-decorator)

(defun my/venv-create-from-current ()
  "Create .venv environement in the current directory from current requirements file using vterm"
  (interactive)
  (buffer-file-name (other-buffer))
  (my/vterm-execute-command (concat which_python " -m venv .venv"));;(setq which_python "python_path") in config.el
  (my/vterm-execute-command "source .venv/bin/activate")
  (my/vterm-execute-command (concat "python -m pip install -r " buffer-file-name))  
  )

(advice-add 'my/venv-create-from-current :around #'my/vterm-other-window-decorator)
