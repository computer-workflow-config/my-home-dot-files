(defun my/ssh ()
  "ssh into my working directory in a locally opened vterm"
  (interactive)
  (dired-other-window "/~");; This is needed if we are in a remote directory or else we get a bad terminal
  (vterm);; To run vterm in the other window pointing at the local home directory in dired
  (other-window -1);; To switch back the window
  (my/vterm-execute-command ssh_user_at_server);;(setq ssh_user_at_server "ssh user@server") in config.el
  (my/vterm-execute-command (concat "cd " (my/pwd)))
  (other-window 1);; To switch to right window containing vterm
  )

(defun my/qlogin ()
  "qlogin into my working directory in a locally opened vterm"
  (interactive)
  (dired-other-window "/~");; This is needed if we are in a remote directory or else we get a bad terminal
  (vterm);; To run vterm in the other window pointing at the local home directory in dired
  (other-window -1);; To switch back the window
  (my/vterm-execute-command ssh_user_at_server);;(setq ssh_user_at_server "ssh user@server") in config.el
  (my/vterm-execute-command "ql");;qlogin alias in the remote .bashrc
  (sleep-for 5)
  (my/vterm-execute-command (concat "cd " (my/pwd)))
  (other-window 1);; To switch to right window containing vterm
  )

