;;Add vterm
(use-package vterm :ensure t)


(defun my/vterm-execute-command (command)
      "Insert command to be executed in vterm"
      (interactive "Insert command to be executed in vterm")
      (require 'vterm)
      (eval-when-compile (require 'subr-x))
        (let ((buf (current-buffer)))
          (unless (get-buffer vterm-buffer-name)
            (vterm))
          (display-buffer vterm-buffer-name t)
          (switch-to-buffer-other-window vterm-buffer-name)
          (vterm--goto-line -1)
          (message command)
          (vterm-send-string command)
          (vterm-send-return)
          (switch-to-buffer-other-window buf)
          ))

(defun my/vterm-other-window-decorator (orig-fun &rest args)
  "Run vterm in another window decorator"
  (dired-other-window "/~");; This is needed if we are in a remote directory or else we get a bad terminal
  (ignore-error void-function
    (vterm));; ignore error Symbol's function definition is void: linum-mode
  ;; To run vterm in the other window pointing at the local home directory in dired
  (other-window -1);; To switch back the window
  (my/vterm-execute-command (concat "cd " (my/pwd)))
  (apply orig-fun args)
  (other-window 1);; To switch to right window containing vterm
  )

(defun my/vterm-other-window ()
  "Run vterm in another window"
  (interactive)
  )

(advice-add 'my/vterm-other-window :around #'my/vterm-other-window-decorator)

;;show/hide vterm in other window
(global-set-key [f9] 'my/vterm-other-window)
