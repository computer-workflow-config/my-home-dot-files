;;Adding a package source
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(add-to-list 'load-path "~/.emacs.d/my_packages/")

;;use-package macro, simplifies the customization and use of packages in Emacs
;;Install use-package if it is not already installed
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(load-file "~/.emacs.d/config.el")
(load-file "~/.emacs.d/load-directory.el")
;;Loads recursively elisp scripts located at the specified directory 
(load-directory "~/.emacs.d/custom1")
;(load-directory "~/.emacs.d/custom-p")

;;Note: put elisp scripts you want to version but don't want to load at:
;;~/.emacs.d/to_version_not_to_load

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(eglot vterm epresent org-bullets fzf smex markdown-mode evil magit openwith auctex pylint treemacs flycheck ledger-mode vertico all-the-icons-dired dired-hide-dotfiles all-the-icons doom-themes use-package mu4e))
 '(warning-suppress-log-types '((comp) (comp)))
 '(warning-suppress-types '((comp))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
