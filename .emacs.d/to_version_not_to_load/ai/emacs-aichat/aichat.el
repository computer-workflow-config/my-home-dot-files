;;https://github.com/xhcoding/emacs-aichat
(add-to-list 'load-path "~/.emacs.d/my_packages/emacs-aichat")

(use-package websocket
  :ensure t
  :config
   )

(use-package async-await
  :ensure t
  :config
   )

;;(require 'aichat)
(require 'aichat-bingai)
;;(require 'aichat-openai)


(defun my/aichat-bingai-append (text)
  "Send the region or input to Bing and append the returned result."
  ;;Read string from region or input
  (interactive (list (aichat-read-region-or-input "Input text: ")))
  (deactivate-mark)
  ;;(insert "\n") ; Add a newline before appending the result
  (aichat-bingai-replace-or-insert text)
  )

(defun my/delete-backtick-lines ()
  "Delete lines with 3 consecutive backticks at the beginning."
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (while (re-search-forward "^```" nil t)
      (let ((start (line-beginning-position))
            (end (line-end-position)))
        (delete-region start end))))
  (message "Deleted lines with 3 consecutive backticks."))

(defun my/aichat-bingai-code-append (text)
  "Send the region or input to Bing and append the returned code."
  ;;Read string from region or input
  (interactive (list (aichat-read-region-or-input "Input text: ")))
  (deactivate-mark)
  ;;(insert "\n") ; Add a newline before appending the result
  (setq prompt_to_append_to_main_prompt "(code and code only, don't say anything but code and no output)")

  (setq final_text (concat text prompt_to_append_to_main_prompt))
  (aichat-bingai-replace-or-insert final_text)
  ;;(run-with-idle-timer 5 nil #'my/delete-backtick-lines)
  )
