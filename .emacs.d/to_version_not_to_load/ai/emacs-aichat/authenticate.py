#https://github.com/xhcoding/emacs-aichat
#pip3 install rookiepy
import rookiepy

list(
    map(
        lambda c: print('{} {} {} {} {} {}'.format(c['name'], c['value'], c[
            'expires'], c['domain'], c['path'], c['secure'])),
        filter(lambda c: c['domain'] in ('.bing.com'),
               rookiepy.edge(['.bing.com']))))
