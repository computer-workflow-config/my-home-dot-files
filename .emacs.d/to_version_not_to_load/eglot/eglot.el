(use-package eglot
   :ensure t
   :config
   ;; Do not forget to install the needed lsp programs
   ;; for example to install pyright:
   ;; sudo npm install -g pyright
   (add-hook 'python-mode-hook #'eglot-ensure)
   )

(use-package company
   :ensure t
   :config
   (global-company-mode)
   )

