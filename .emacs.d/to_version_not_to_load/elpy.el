;;Use Elpy which is an extension for the Emacs text editor to work with Python projects
;;Prerequisites
;;python3-venv terminal package should be installed so the default .venv can created by elpy if it is not existing
;;The following error can sometimes appear
;;error in process sentinel: elpy-rpc--default-error-callback: peculiar error: "exited abnormally with code 1" error in process sentinel: peculiar error: "exited abnormally with code 1"
;;If the above error appears run elpy-rpc-reinstall-virtualenv (knowing that python3-venv is already installed)
;;When running a .venv python env using pyvenv-activate. ".venv" should be explicitly written in the path of to the environmenr
(use-package elpy
  :ensure t
  :init
  ;;Enable elpy by default for python files
  (elpy-enable)
  :config
  
  ;;For further elpy configuration: M-x elpy-config


  ;;Use flycheck in elpy instead of flymake
  ;;Preliminary requirement: download flycheck
  ;; (when (load "flycheck" t t)
  ;;    (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
  ;;    (add-hook 'elpy-mode-hook 'flycheck-mode))


  ;;Enable flycheck mode but don't enable flycheck mode for remote buffers
  ;;Preliminary requirement: download flycheck
  (when (load "flycheck" t t)
    (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
    (add-hook 'elpy-mode-hook 'jj/flycheck-mode))
  (defun jj/flycheck-mode ()
    "Don't enable flycheck mode for remote buffers."
    (interactive)
    (if (file-remote-p default-directory)
	(flycheck-mode -1)
      (flycheck-mode t)))
  )
